# keycloak-user-ip-authenticator

This Keycloak custom Authenticator verifies the remote IP address of the users and deny
authentication if their remote IP is not within the declared white listed IP addresses.

There are several ways to declare white listed IP addresses.

- A global white list that is applied to _all_ users
- Per user white list

## IP address sources

Currently, the custom authenticator supports three different sources of providing 
IP addresses.

- A global white list can be provided in the authenticator's configuration 
`Whitelisted IP Addresses`. This must be a space delimited list of IP addresses and/or
 CIDRs

- Using user attributes in Keycloak. The name of the attribute that provides the IP 
  address can be configured using `Name of IP Address attribute` config parameter. 

- An external file that provides a list of IP addresses for each user. The path to this 
  file can be configured using config parameter 
  `Absolute path to file containing users and their declared IP Addresses` in Keycloak
  web interface.

If there are multiple sources configured, IP addresses from each source will be fetched 
for a given user and then merged to verify them against the current remote IP address.

## IP address file source

A [sample file](./sample-ipaddr.conf) is provided in the repository. A typical line 
in that file is as follows:

```
+ | <user> | <space delimited IP addresses or CIDR>
```

The first qualifier in the line `+` says that this user is allowed to authenticate. The 
second qualifier is the username and the last one is space delimited IP addresses or 
CIDRs. 

There is special user qualifier `ALL` which means all the users are allowed to 
authenticate from those IP addresses.

In a typical scenario, we will gather the IP addresses and/or CIDRs of `ALL` and 
specific user `<user>` and check if the remote IP address is within those IP addresses.

## Build

The Keycloak SPI is very stable but always make sure that Keycloak SPI dependencies and 
your Keycloak server versions match. Keycloak SPI dependencies version is 
configured in `pom.xml` in the `keycloak.version` property.

To build the project execute the following command:

```bash
mvn package
```

## Deploy

Assuming `$KEYCLOAK_HOME` is pointing to you Keycloak installation.

If you use legacy Keycloak running on WildFly copy it into deployments directory:
 
```bash
cp target/keycloak-ip-authenticator.jar $KEYCLOAK_HOME/standalone/deployments/
```

If you use latest Keycloak running on Quarkus copy it into providers directory:

```bash
cp target/keycloak-ip-authenticator.jar $KEYCLOAK_HOME/providers/
```
