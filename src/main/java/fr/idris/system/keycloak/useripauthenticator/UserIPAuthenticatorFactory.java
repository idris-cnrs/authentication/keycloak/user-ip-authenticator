package fr.idris.system.keycloak.useripauthenticator;

// import static org.keycloak.provider.ProviderConfigProperty.STRING_TYPE;

import static java.util.Arrays.asList;
import java.util.List;

import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

public class UserIPAuthenticatorFactory implements AuthenticatorFactory {

    public static final String PROVIDER_ID = "user-ip-authenticator";
    private static final Authenticator SINGLETON = new UserIPAuthenticator();
    static final String ALLOWED_IP_ADDRESS_CONFIG = "allowed.ip.address.file";
    static final String WHITELISTED_IP_ADDRESSES = "allowed.ip.addresses";
    static final String IP_ADDRESS_ATTRIBUTE_NAME = "ip.address.attribute";
    static final String DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME = "ipAddress";

    @Override
    public Authenticator create(KeycloakSession keycloakSession) {
        return SINGLETON;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    private static AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
            AuthenticationExecutionModel.Requirement.REQUIRED,
            AuthenticationExecutionModel.Requirement.ALTERNATIVE,
            AuthenticationExecutionModel.Requirement.DISABLED
    };
    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }

    @Override
    public boolean isConfigurable() {
        return true;
    }

    @Override
    public boolean isUserSetupAllowed() {
        return false;
    }

    @Override
    public String getDisplayType() {
        return "User IP Authenticator";
    }

    @Override
    public String getHelpText() {
        return "Limits access to users from their declared IP Addresses";
    }

    @Override
    public List<ProviderConfigProperty> getConfigProperties() {
        // Config option to provide attribute name
        ProviderConfigProperty attributeNameProperty = new ProviderConfigProperty();
        attributeNameProperty.setType(ProviderConfigProperty.STRING_TYPE);
        attributeNameProperty.setName(IP_ADDRESS_ATTRIBUTE_NAME);
        attributeNameProperty.setDefaultValue(DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME);
        attributeNameProperty.setLabel("Name of IP Address attribute");
        attributeNameProperty.setHelpText("This attribute can be multi-valued");

        // Config option to provide a list of whitelisted IP addresses
        ProviderConfigProperty whitelistedIPAdressProperty = new ProviderConfigProperty();
        whitelistedIPAdressProperty.setType(ProviderConfigProperty.STRING_TYPE);
        whitelistedIPAdressProperty.setName(WHITELISTED_IP_ADDRESSES);
        whitelistedIPAdressProperty.setLabel("Whitelisted IP Addresses");
        whitelistedIPAdressProperty.setHelpText("List of IP addresses that are allowed for all users delimited by space");

        // Config option to set path to find file containing users and their declared IP addresses
        ProviderConfigProperty ipAddressFileProperty = new ProviderConfigProperty();
        ipAddressFileProperty.setType(ProviderConfigProperty.STRING_TYPE);
        ipAddressFileProperty.setName(ALLOWED_IP_ADDRESS_CONFIG);
        ipAddressFileProperty.setLabel("Absolute path to file containing users and their declared IP Addresses");
        ipAddressFileProperty.setHelpText("Ensure that user running Keycloak service has permissions to read the file");
        return asList(whitelistedIPAdressProperty, attributeNameProperty, ipAddressFileProperty);
    }

    @Override
    public String getReferenceCategory() {
        return null;
    }

    @Override
    public void init(Config.Scope scope) {
    }

    @Override
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {
    }

    @Override
    public void close() {
    }
}
