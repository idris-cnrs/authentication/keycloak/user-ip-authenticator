/*
 This is the custom authenticator for authenticating the IP addresses of the
 users from which they are connecting from. 
 
 IP addresses of users can be provided using different sources
 
 - A global white listed addresses that can be configured for the authenticator. Multiple
   IP addresses in either single IP or CIDR format delimited by space can be configured

 - IP addresses from user attributes. The attribute that provides the IP address of 
   user can be configured for the authenticator.
 
 - A file with list of users and the allowed addresses for each user. The path to the 
   file can be configured with the authenticator. This file can be updated in real time
   as every authentication flow will open and read file in a separate file handler.
   
 Either one or more of these sources can be used together. IP addresses found from 
 each of these sources will be merged and then compared against remote IP address of 
 the client.
*/
package fr.idris.system.keycloak.useripauthenticator;

import java.io.IOException;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.jboss.logging.Logger;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.commons.net.util.SubnetUtils.SubnetInfo;

import jakarta.ws.rs.core.Response;

import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.events.Errors;
import org.keycloak.models.*;

public class UserIPAuthenticator implements Authenticator {

    private static final Logger logger = Logger.getLogger(UserIPAuthenticator.class);

    @Override
    public void authenticate(AuthenticationFlowContext context) {
        RealmModel realm = context.getRealm();
        UserModel user = context.getUser();

        String username = user.getUsername();
        String realmname = realm.getName();
        String remoteIPAddress = context.getConnection().getRemoteAddr();
        logger.infof("User %s in realm %s is connecting from Remote IP %s", username, realmname, remoteIPAddress);
        List<String> allowedIPAddresses = getAllowedIPAddress(user, context);
        logger.infof("Declared IP addresses for user %s in realm %s: %s", username, realmname,
                allowedIPAddresses);

        for (String subnet : allowedIPAddresses) {
            if (isInSubnet(remoteIPAddress, subnet)) {
                logger.infof("Authorizing user %s in realm %s. Remote IP address: %s Declared IP address(es): %s", username, realmname, remoteIPAddress, subnet);
                context.success();
                return;
            }
        }
        
        logger.infof("Denying user %s in realm %s. Remote IP address: %s Declared IP address(es): %s", username, realmname, remoteIPAddress, allowedIPAddresses);
        String errorMessage = String.format("User's remote IP %s is not in their declared IP addresses", remoteIPAddress);
        context.getEvent().error(Errors.ACCESS_DENIED);
        Response response = context.form()
                .setError(errorMessage)
                .createErrorPage(Response.Status.UNAUTHORIZED);
        context.failure(AuthenticationFlowError.ACCESS_DENIED, response);
    }

    private List<String> getAllowedIPAddress(UserModel user, AuthenticationFlowContext context) {
        // Get config
        Map<String, String> config = context.getAuthenticatorConfig().getConfig();
        String ipAddressAttributeName = config.get(UserIPAuthenticatorFactory.IP_ADDRESS_ATTRIBUTE_NAME);
        String whitelistedIPAddresses = config.get(UserIPAuthenticatorFactory.WHITELISTED_IP_ADDRESSES);
        String ipAddressFile = config.get(UserIPAuthenticatorFactory.ALLOWED_IP_ADDRESS_CONFIG);

        // Initialise IP addresses map
        List<String> ipAddresses = new ArrayList<>();

        // Append whitelisted IP addresses to user specific IP addresses
        if (whitelistedIPAddresses != null && !whitelistedIPAddresses.trim().isEmpty()) {
            for (String ip : whitelistedIPAddresses.split(" ")) {
                ipAddresses.add(ip.trim());
            }
        }

        // First check if ipAddress attribute is available in UserModel and get values
        // of it if available
        if (ipAddressAttributeName != null && !ipAddressAttributeName.trim().isEmpty()) {
            for (var entry : user.getAttributes().entrySet()) {
                // If ipAddress attribute is found in user attributes, add them to ipAddresses
                // list
                if (entry.getKey().equals(ipAddressAttributeName)) {
                    for (String ip : entry.getValue()) {
                        ipAddresses.add(ip.trim());
                    }
                }
            }
        }

        // Finally check the existence of ipAdressFile and merge the Ip addresses
        // found in the file to list
        if (ipAddressFile != null && !ipAddressFile.trim().isEmpty()) {
            List<String> allLines = readIPAdressFile(ipAddressFile);
            for (String ip : parseUserIPAddresses(user.getUsername(), allLines)) {
                ipAddresses.add(ip);
            }
        }
        return ipAddresses;
    }

    private List<String> parseUserIPAddresses(String username, List<String> contents) {
        List<String> ipAddresses = new ArrayList<>();
        for (String line : contents) {
            String[] lineList = line.split("\\|");
            if (lineList.length == 3) {
                if (lineList[0].trim().equals("+") && (lineList[1].trim().equals(username) || lineList[1].trim().equals("ALL"))) {
                    for (String ip : lineList[2].trim().split(" ")) {
                        ipAddresses.add(ip.trim());
                    }
                }
            }
        }
        return ipAddresses;
    }

    private List<String> readIPAdressFile(String filePath) {
        try {
            Path path = Paths.get(filePath);
            return Files.readAllLines(path);
        } catch(IOException ex) {
            ex.printStackTrace();
            logger.errorf("Neither IP Address attribute nor a file at %s containing IP addresses of users found",
                    filePath);
            return new ArrayList<>();
        }
    }

    private boolean isInSubnet (String ip, String subnet) {
        // First check if it is really a subnet
        // If it is a single IP address just compare it with remote Ip directly
        if (subnet.indexOf("/") == -1) {
            return subnet.equals(ip);
        }
        // If it is single IP address in CIDR format, strip /32 and compare
        if (subnet.contains("/32")) {
            return subnet.split("/32")[0].equals(ip);
        }
        // In other cases use SubnetUtils to check if IP is in subnet
        SubnetUtils utils = new SubnetUtils(subnet);
        SubnetInfo info = utils.getInfo();
        return info.isInRange(ip);
    }

    @Override
    public void action(AuthenticationFlowContext context) {
    }

    @Override
    public boolean requiresUser() {
        return true;
    }

    @Override
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return true;
    }

    @Override
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
    }

    @Override
    public void close() {
    }
}
