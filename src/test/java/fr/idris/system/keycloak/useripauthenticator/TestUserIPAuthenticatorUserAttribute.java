/*
 Unit tests for user-ip-authenticator to test some basic cases
*/
package fr.idris.system.keycloak.useripauthenticator;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.keycloak.common.ClientConnection;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.events.EventBuilder;
import org.keycloak.models.*;

import jakarta.ws.rs.core.Response;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUserIPAuthenticatorUserAttribute {

    private Authenticator authenticator;

    @Mock
    private AuthenticationFlowContext context;

    @Mock
    private AuthenticatorConfigModel configModel;

    @Mock
    private ClientConnection clientConnection;

    @Mock
    private LoginFormsProvider form;

    @Mock
    private Response response;

    @Mock
    private EventBuilder event;

    @Mock
    private RealmModel realmModel;

    @Mock
    private UserModel userModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        authenticator = new UserIPAuthenticator();

        Map<String, String> config = new HashMap<String, String>();
        config.put(UserIPAuthenticatorFactory.IP_ADDRESS_ATTRIBUTE_NAME,
                UserIPAuthenticatorFactory.DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME);

        when(context.getRealm()).thenReturn(realmModel);
        when(context.getUser()).thenReturn(userModel);
        when(context.getConnection()).thenReturn(clientConnection);
        when(context.getAuthenticatorConfig()).thenReturn(configModel);
        when(context.form()).thenReturn(form);
        when(context.getEvent()).thenReturn(event);
        when(realmModel.getName()).thenReturn("test-realm");
        when(configModel.getConfig()).thenReturn(config);
    }

    @Test
    public void testSuccessWithIPAddress() throws Exception {
        when(userModel.getUsername()).thenReturn("foo001");
        when(clientConnection.getRemoteAddr()).thenReturn("10.89.0.4");

        List<String> ipAddresses = new ArrayList<>();
        ipAddresses.add("10.89.0.0/16");
        Map<String, List<String>> userAttributes = new HashMap<String, List<String>>();
        userAttributes.put(UserIPAuthenticatorFactory.DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME, ipAddresses);
        when(userModel.getAttributes()).thenReturn(userAttributes);

        authenticator.authenticate(context);
        verify(context).success();
    }

    @Test
    public void testSuccessWithIPAddressWhitelistAttribute() throws Exception {
        when(userModel.getUsername()).thenReturn("foo001");
        when(clientConnection.getRemoteAddr()).thenReturn("10.89.0.4");

        // Set white listed IP addresses
        Map<String, String> config = new HashMap<String, String>();
        config.put(UserIPAuthenticatorFactory.WHITELISTED_IP_ADDRESSES, "10.89.0.0/16");
        when(configModel.getConfig()).thenReturn(config);

        // Set empty user IP addresses
        ArrayList<String> ipAddresses = new ArrayList<>();
        when(userModel.getAttributeStream(UserIPAuthenticatorFactory.IP_ADDRESS_ATTRIBUTE_NAME))
                .thenReturn(ipAddresses.stream());

        authenticator.authenticate(context);
        verify(context).success();
    }

    @Test
    public void testSuccessWithSingleIPAddress() throws Exception {
        when(userModel.getUsername()).thenReturn("foo002");
        when(clientConnection.getRemoteAddr()).thenReturn("10.89.0.2");

        List<String> ipAddresses = new ArrayList<>();
        ipAddresses.add("10.89.0.2");
        Map<String, List<String>> userAttributes = new HashMap<String, List<String>>();
        userAttributes.put(UserIPAuthenticatorFactory.DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME, ipAddresses);
        when(userModel.getAttributes()).thenReturn(userAttributes);

        authenticator.authenticate(context);
        verify(context).success();
    }

    @Test
    public void testFailureWithEmptyIPAddressAttribute() throws Exception {
        String remoteIPAddress = "10.89.0.4";
        String errorMessage = String.format("User's remote IP %s is not in their declared IP addresses",
                remoteIPAddress);
        when(userModel.getUsername()).thenReturn("foo001");
        when(clientConnection.getRemoteAddr()).thenReturn(remoteIPAddress);

        // Set user IP addresses
        ArrayList<String> ipAddresses = new ArrayList<>();
        Map<String, List<String>> userAttributes = new HashMap<String, List<String>>();
        userAttributes.put(UserIPAuthenticatorFactory.DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME, ipAddresses);
        when(userModel.getAttributes()).thenReturn(userAttributes);
        when(form.setError(errorMessage)).thenReturn(form);

        authenticator.authenticate(context);
        verify(context).failure(AuthenticationFlowError.ACCESS_DENIED,
                form.createErrorPage(Response.Status.UNAUTHORIZED));
    }

    @Test
    public void testFailureWithEmptyConfig() throws Exception {
        String remoteIPAddress = "10.89.0.4";
        String errorMessage = String.format("User's remote IP %s is not in their declared IP addresses",
                remoteIPAddress);
        when(userModel.getUsername()).thenReturn("foo001");
        when(clientConnection.getRemoteAddr()).thenReturn(remoteIPAddress);

        // Set user IP addresses
        Map<String, String> config = new HashMap<String, String>();
        when(configModel.getConfig()).thenReturn(config);
        when(form.setError(errorMessage)).thenReturn(form);

        authenticator.authenticate(context);
        verify(context).failure(AuthenticationFlowError.ACCESS_DENIED,
                form.createErrorPage(Response.Status.UNAUTHORIZED));
    }
}
