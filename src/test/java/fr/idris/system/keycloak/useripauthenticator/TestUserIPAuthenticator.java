/*
 Unit tests for user-ip-authenticator to test some basic cases
*/
package fr.idris.system.keycloak.useripauthenticator;

import org.junit.Before;
import org.junit.Test;

import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.io.File;

import org.keycloak.common.ClientConnection;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.events.EventBuilder;
import org.keycloak.models.*;

import jakarta.ws.rs.core.Response;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUserIPAuthenticator {

    private Authenticator authenticator;

    @Mock
    private AuthenticationFlowContext context;

    @Mock
    private AuthenticatorConfigModel configModel;

    @Mock
    private ClientConnection clientConnection;

    @Mock
    private LoginFormsProvider form;

    @Mock
    private Response response;

    @Mock
    private EventBuilder event;

    @Mock
    private RealmModel realmModel;

    @Mock
    private UserModel userModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        File ipAddressFile = new File(this.getClass().getResource("/access-ssh.conf").getFile());

        authenticator = new UserIPAuthenticator();

        Map<String, String> config = new HashMap<String, String>();
        config.put(UserIPAuthenticatorFactory.ALLOWED_IP_ADDRESS_CONFIG, ipAddressFile.getAbsolutePath());
        config.put(UserIPAuthenticatorFactory.IP_ADDRESS_ATTRIBUTE_NAME,
                UserIPAuthenticatorFactory.DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME);

        List<String> ipAddresses = new ArrayList<>();
        Map<String, List<String>> userAttributes = new HashMap<String, List<String>>();
        userAttributes.put(UserIPAuthenticatorFactory.DEFAULT_IP_ADDRESS_ATTRIBUTE_NAME, ipAddresses);

        when(context.getRealm()).thenReturn(realmModel);
        when(context.getUser()).thenReturn(userModel);
        when(context.getConnection()).thenReturn(clientConnection);
        when(context.getAuthenticatorConfig()).thenReturn(configModel);
        when(context.form()).thenReturn(form);
        when(context.getEvent()).thenReturn(event);
        when(realmModel.getName()).thenReturn("test-realm");
        when(configModel.getConfig()).thenReturn(config);

        when(userModel.getAttributes()).thenReturn(userAttributes);
    }

    @Test
    public void testSuccessWithIPAddress() throws Exception {
        when(userModel.getUsername()).thenReturn("foo001");
        when(clientConnection.getRemoteAddr()).thenReturn("10.89.0.4");

        authenticator.authenticate(context);
        verify(context).success();
    }

    @Test
    public void testSuccessWithSingleIPAddress() throws Exception {
        when(userModel.getUsername()).thenReturn("foo002");
        when(clientConnection.getRemoteAddr()).thenReturn("10.89.0.2");

        authenticator.authenticate(context);
        verify(context).success();
    }

    @Test
    public void testSuccessWithSingleIPAddressInCIDR() throws Exception {
        when(userModel.getUsername()).thenReturn("foo004");
        when(clientConnection.getRemoteAddr()).thenReturn("10.0.2.111");

        authenticator.authenticate(context);
        verify(context).success();
    }

    @Test
    public void testFailure() throws Exception {
        String remoteIPAddress = "10.1.1.1";
        String errorMessage = String.format("User's remote IP %s is not in their declared IP addresses",
                remoteIPAddress);

        when(userModel.getUsername()).thenReturn("bar001");
        when(clientConnection.getRemoteAddr()).thenReturn(remoteIPAddress);
        when(form.setError(errorMessage)).thenReturn(form);

        authenticator.authenticate(context);
        verify(context).failure(AuthenticationFlowError.ACCESS_DENIED, form.createErrorPage(Response.Status.UNAUTHORIZED));
    }

    @Test
    public void testFailureWhenUserIPAddressNotFound() throws Exception {
        String remoteIPAddress = "10.1.1.1";
        String errorMessage = String.format("User's remote IP %s is not in their declared IP addresses",
                remoteIPAddress);

        when(userModel.getUsername()).thenReturn("nonexistant");
        when(clientConnection.getRemoteAddr()).thenReturn(remoteIPAddress);
        when(form.setError(errorMessage)).thenReturn(form);

        authenticator.authenticate(context);
        verify(context).failure(AuthenticationFlowError.ACCESS_DENIED, form.createErrorPage(Response.Status.UNAUTHORIZED));
    }
}
